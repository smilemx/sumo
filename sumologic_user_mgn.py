import json
import requests
import time
import random
import datetime
import sys
import os, ssl
from dateutil import parser
try:
    import cookielib
except ImportError:
    import http.cookiejar as cookielib

DEFAULT_VERSION = 'v1'
SUMO_ACCESS_ID="enter id"
SUMO_ACCESS_KEY="enter key"
EP="https://api.au.sumologic.com/api/v1/users"

if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
getattr(ssl, '_create_unverified_context', None)):
    ssl._create_default_https_context = ssl._create_unverified_context

class SumoLogic(object):

    def __init__(self, accessId, accessKey, endpoint=None, cookieFile='cookies.txt'):
        self.session = requests.Session()
        self.session.auth = (accessId, accessKey)
        self.session.headers = {'content-type': 'application/json', 'accept': 'application/json'}
        cj = cookielib.FileCookieJar(cookieFile)
        self.session.cookies = cj
        if endpoint is None:
            self.endpoint = self._get_endpoint()
        else:
            self.endpoint = endpoint
        if self.endpoint[-1:] == "/":
            raise Exception("Endpoint should not end with a slash character")

    def _get_endpoint(self):
        """
        SumoLogic REST API endpoint changes based on the geo location of the client.
        For example, If the client geolocation is Australia then the REST end point is
        https://api.au.sumologic.com/api/v1
        When the default REST endpoint (https://api.sumologic.com/api/v1) is used the server
        responds with a 401 and causes the SumoLogic class instantiation to fail and this very
        unhelpful message is shown 'Full authentication is required to access this resource'
        This method makes a request to the default REST endpoint and resolves the 401 to learn
        the right endpoint
        """
        self.endpoint = 'https://api.au.sumologic.com/api'
        self.response = self.session.get('https://api.au.sumologic.com/api/v1/users')  # Dummy call to get endpoint
        endpoint = self.response.url.replace('/v1/users', '')  # dirty hack to sanitise URI and retain domain
        print("SDK Endpoint", endpoint)
        print("\n\n")
        return endpoint

    def get_versioned_endpoint(self, version):
        return self.endpoint + '/%s' % version

    def delete(self, method, params=None, version=DEFAULT_VERSION):
        endpoint = self.get_versioned_endpoint(version)
        r = self.session.delete(endpoint + method, params=params)
        if 400 <= r.status_code < 600:
            r.reason = r.text
        r.raise_for_status()
        return r

    def get(self, method, params=None, version=DEFAULT_VERSION):
        endpoint = self.get_versioned_endpoint(version)
        r = self.session.get(endpoint + method, params=params)
        if 400 <= r.status_code < 600:
            r.reason = r.text
        r.raise_for_status()
        return r

    def post(self, method, params, headers=None, version=DEFAULT_VERSION):
        endpoint = self.get_versioned_endpoint(version)
        r = self.session.post(endpoint + method, data=json.dumps(params), headers=headers)
        if 400 <= r.status_code < 600:
            r.reason = r.text
        r.raise_for_status()
        return r

    def put(self, method, params, headers=None, version=DEFAULT_VERSION):
        endpoint = self.get_versioned_endpoint(version)
        r = self.session.put(endpoint + method, data=json.dumps(params), headers=headers)
        if 400 <= r.status_code < 600:
            r.reason = r.text
        r.raise_for_status()
        return r

    def list_users(self, limit):
        result_limi = {"limit":limit}
        r = self.get('/users',result_limi)
        return json.loads(r.text)['data']

    def disable_user(self, user_id, fname, lname, roles):
        user_data = {'firstName':fname,'lastName':lname,'isActive':'false','roleIds':roles}
        r = self.put('/users/%s' % user_id, user_data)
        print(r.text)

def list_active(sm_obj, log_file):
    print("List users... ")
    user_list = sm_obj.list_users(1000)
    users_filename="./log/user_list_" + str(datetime.datetime.now().strftime('%Y%m%d_%H%M%S'))
    with open(users_filename, "w") as ufile:
        json.dump(user_list,ufile)
        print("Active users file created ! ")

def disable_inactive(sm_obj, log_file):
    count_active = 0
    count_inactive = 0
    # check each user
    user_list = sm_obj.list_users(1000)
    for i in user_list:
        if (i['lastLoginTimestamp'] != None):
            login_time = parser.parse(i['lastLoginTimestamp'])
            login_time = login_time.replace(tzinfo=None)
            login_gap = datetime.datetime.now() - login_time
            if (login_gap > datetime.timedelta(days=365)):
                if (i['isActive'] == True):
                    log_file.write("User " + i["firstName"] + " " + i["lastName"] + " is active " + i["id"] +"    Disabling this user ...\n")
                    sm.disable_user(i['id'], i['firstName'], i['lastName'], i['roleIds'] )
                    count_active = count_active + 1
                else:
                    count_inactive = count_inactive + 1

    log_file.write("There are total of " + str(count_active) + " users disabled\n\n")
    print("There are total of " + str(count_active) + " users disabled")

# Main Function for Menu-Driven
def main():
    if (sys.version_info[0] < 3):
        SUMO_ACCESS_ID = raw_input("Please enter access id: ")
        SUMO_ACCESS_KEY = raw_input("Please enter access key: ")
    else:
        SUMO_ACCESS_ID = input("Please enter access id: ")
        SUMO_ACCESS_KEY = input("Please enter access key: ")

    sm = SumoLogic(SUMO_ACCESS_ID,SUMO_ACCESS_KEY)
    log_file_name = "./log/disable_hist.log"
    logfile = open(log_file_name,"a")
    logfile.write(str(datetime.datetime.now()) + "\n")

    while True:
        print("\nMenu Option : ")
        print("Create File To List All Active Users  (1)")
        print("Disable Inactive Users Not Login More Than One Year  (2)")
        print("Exit  (3)\n")
        if (sys.version_info[0] < 3):
            choice = raw_input("Please enter option: ")
        else:
            choice = input("Please enter option: ")

        if choice=="1":
            list_active(sm, logfile)
        elif choice=="2":
            disable_inactive(sm, logfile)
        elif choice=="3":
            break
        else:
            print("Invalid choice, please choose again\n")

    logfile.close()
    sys.exit("Program completed !")

if __name__ == "__main__":
    print("----------------------"+\
        "Sumo User Management "+\
        "----------------------")

main()